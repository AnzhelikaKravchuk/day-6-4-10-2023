#include <iostream>

using namespace std;

int main()
{
    double product = 1;
    int n;
    cout << "Enter n: ";
    cin >> n;

    double i = 1;

    while(i <= n) 
    {
        product *= 1 + 1. / i / i;
        i++;
    }

    cout << "product = " << product << endl;
}
