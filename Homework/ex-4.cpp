#include <iostream>

using namespace std;

int main()
{
    double sum = 0;
    int n;
    cout << "Enter n: ";
    cin >> n;

    double i = 1;

    while (i <= n)
    {
        sum += pow(-1, i) / (2*i + 1);
        i++;
    }

    cout << "number = " << sum << endl;
}
