#include <iostream>

using namespace std;

int main()
{
    double sum = 0;
    int n;
    cout << "Enter n: ";
    cin >> n;

    double i = 1;

    while (i <= n)
    {
        sum += 1. / i;
        i++;
    }

    cout << "sum = " << sum << endl;
}
