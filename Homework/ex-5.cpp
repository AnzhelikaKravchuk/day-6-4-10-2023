#include <iostream>

using namespace std;

int main()
{
    double result = sqrt(2);
    int n;
    cout << "Enter n: ";
    cin >> n;

    double i = 1;

    while (i < n)
    {
        result = sqrt(2. + result);
        i++;
    }

    cout << "number = " << result << endl;
}
